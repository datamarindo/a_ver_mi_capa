#!/usr/bin/env python3

# MENCIONAR QUE CONTEXTILY SE TIENE QUE INSTALAR LA VERSIÓN MÁS NUEVA
# TODO: REMOVER LA CAPA TIF QUE SE GENERA DESPUÉS DE PLOTEAR
import geopandas as gp
import contextily as ctx
import matplotlib.pyplot as plt
import fiona
import argparse

fiona.drvsupport.supported_drivers['kml'] = 'rw' # pq geopandas no lee kml por defecto
fiona.drvsupport.supported_drivers['KML'] = 'rw'

# tuneando la help para la terminal
class CapitalisedHelpFormatter(argparse.HelpFormatter):
   def add_usage(self, usage, actions, groups, prefix=None):
       if prefix is None:
           prefix = 'Modo de empleo: '
       return super(CapitalisedHelpFormatter, self).add_usage(
           usage, actions, groups, prefix)

parser=argparse.ArgumentParser(formatter_class=CapitalisedHelpFormatter,
    description='''Función para explorar capas vectoriales sin abrir QGIS u otro SIG en Linux shell. Requiere la versión más reciente de contextily (pip3 install contextily==1.0rc2); requiere geopandas, fiona, matplotlib y argparse''',
    epilog="""Casabe Díaz, %(prog)s, https://gitlab.com/datamarindo.""", add_help = False)
parser.add_argument('archivo', type = str, help = "Nombre del archivo (GPKG, shp, kml, geojson, etc)")
parser.add_argument('-zoom', type = str, default = 6, help='zoom de 3 (más lejano) a 16 (más cercano), default = 10')
parser.add_argument('-titulo', help = "Título del plot", default = "pasar '-titulo = mi_titulo'a los args", type = str  )
parser.add_argument('-h', '--help', action='help', default=argparse.SUPPRESS,
                    help='Mostrar este mensaje y salir.')
parser._positionals.title = 'Argumentos posicionales'
parser._optionals.title = 'Argumentos opcionales'
args=parser.parse_args()

# tomar argumentos de terminal stdin
file = args.archivo
encuadre = args.zoom
titulo = args.titulo

# leer capa, descargar tiles y convertir a 4326 y plotear
capa = gp.read_file(file).to_crs(epsg = 3857)
print("Campos de la capa:", capa.keys())
w, s, e, n = capa.total_bounds
img, ext = ctx.bounds2raster(w, s, e, n, zoom = int(encuadre), path = 'warp_tst.tif', url = 'http://tile.stamen.com/terrain-background/tileZ/tileX/tileY.png') # zoom está quedando "auto" si no se indica

capa_wm = capa.to_crs(epsg = 4326)
ax = capa_wm.plot(figsize = (5,5),  facecolor="none", edgecolor='black')#color = "red", alpha = 0.5)

ctx.add_basemap(ax, crs = {"init": "epsg:4326"}, url = "warp_tst.tif", attribution = None)
ax.set_title(titulo)
plt.box(False)
#ax.arrow( -96.0, 18.4, 0.0, 0.05, fc="k", ec="k", head_width=0.05, head_length=0.1 )
plt.show()

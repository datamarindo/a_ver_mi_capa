# Para ver capas vectoriales (gpkg, shp, kml) sin necesidad de QGIS u otro SIG

## Muestra
![Imagen del plot](image.png)

## Preinstalación:
sudo apt install python3-pip

sudo pip3 install numpy

sudo pip3 install geopandas

sudo pip3 install matplotlib

sudo pip3 install contextily==1.0rc2

## Instalación
1. Clonar el repositorio o descargar el archivo .py
2. Ubicar un directorio (path) con ```echo $PATH``` o crear uno
3. Copiar el archivo a ese directorio
4. Ya se puede llamar directamente desde la terminal sin especificar su ubicación

# Uso
ejemplo:
```a_ver_mi_capa.py mis_poligonos.gpkg -zoom 8 -titulo "mis polígonos"```
Nota: mientras más grande sea el zoom más tarda en cargar los tiles

Para imprimir la ayuda usar ```-h```
__También se puede configurar que con doble click se imprima el mapa, dando click derecho a un archivo gpkg y propiedades__

![shell](shell.png)